# LovelyOffline

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

---

## Further info

`firebase init`

### Feature Selection

- Firestore
- Functions
- Hosting

### Build location

"dist"

`firebase deploy`

```bash
=== Deploying to 'angular-pwa-cdfe2'...

i  deploying firestore, functions, hosting
Running command: npm --prefix "$RESOURCE_DIR" run lint

> functions@ lint /Users/Freddy/angular/lovely-offline/functions
> eslint .

✔  functions: Finished running predeploy script.
i  firestore: checking firestore.rules for compilation errors...
i  firestore: reading indexes from firestore.indexes.json...
✔  firestore: rules file firestore.rules compiled successfully
i  functions: ensuring necessary APIs are enabled...
✔  functions: all necessary APIs are enabled
i  firestore: uploading rules firestore.rules...
✔  firestore: deployed indexes in firestore.indexes.json successfully
i  functions: preparing functions directory for uploading...
i  hosting[angular-pwa-cdfe2]: beginning deploy...
i  hosting[angular-pwa-cdfe2]: found 10 files in dist
✔  hosting[angular-pwa-cdfe2]: file upload complete
✔  firestore: released rules firestore.rules to cloud.firestore
i  hosting[angular-pwa-cdfe2]: finalizing version...
✔  hosting[angular-pwa-cdfe2]: version finalized
i  hosting[angular-pwa-cdfe2]: releasing new version...
✔  hosting[angular-pwa-cdfe2]: release complete

✔  Deploy complete!

Please note that it can take up to 30 seconds for your updated functions to propagate.
Project Console: https://console.firebase.google.com/project/angular-pwa-cdfe2/overview
Hosting URL: https://angular-pwa-cdfe2.firebaseapp.com
```

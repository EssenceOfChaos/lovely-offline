// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyD0lB79pLRLXawo-DdCzdwUtyG1vhsh-0o',
    authDomain: 'angular-pwa-cdfe2.firebaseapp.com',
    databaseURL: 'https://angular-pwa-cdfe2.firebaseio.com',
    projectId: 'angular-pwa-cdfe2',
    storageBucket: 'angular-pwa-cdfe2.appspot.com',
    messagingSenderId: '28295398981',
    appId: '1:28295398981:web:3336bd26c99d4681'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
